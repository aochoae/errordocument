FROM php:7.1-apache

RUN a2enmod rewrite

RUN echo "<?php phpinfo();" > /var/www/html/index.php
RUN echo "<?php header('HTTP/1.0 403 Forbidden'); echo \"Error 403\";" > /var/www/html/403.php

COPY dest/ /var/www/html/

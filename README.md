# Custom Error Responses

Customized error responses can be defined for any HTTP status code designated
as an error condition, that is, any 4xx or 5xx status.

[Documentation](https://httpd.apache.org/docs/2.4/en/custom-error.html)

## Build

    npm install --global gulp-cli
    npm install --silent
    gulp

## Docker (Testing)

    docker build -t errordocument .
    docker run -p 8888:80 -d errordocument

## Credits

* Icon made by [Freepik](https://www.flaticon.com)
  from [www.flaticon.com](https://www.flaticon.com/free-icon/browser_309190)

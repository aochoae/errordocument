// Gulp.js configuration

const gulp = require('gulp'),
      gulpRename = require('gulp-rename'),
      gulpHtml = require('gulp-htmlmin');

function htmlmin() {
    return gulp.src('src/error/*.shtml')
        .pipe(gulpHtml({
            html5: true,
            removeComments: true,
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('dest/error'));
}

function rename() {
    return gulp
        .src('src/htaccess')
        .pipe(gulpRename('.htaccess'))
        .pipe(gulp.dest('dest'));
}

exports.default = gulp.parallel(htmlmin, rename);
